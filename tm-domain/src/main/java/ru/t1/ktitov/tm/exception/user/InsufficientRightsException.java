package ru.t1.ktitov.tm.exception.user;

public final class InsufficientRightsException extends AbstractUserException {

    public InsufficientRightsException() {
        super("Error! You don't have enough rights.");
    }

}
