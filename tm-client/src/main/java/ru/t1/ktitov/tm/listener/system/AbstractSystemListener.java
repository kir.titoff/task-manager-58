package ru.t1.ktitov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.ktitov.tm.listener.AbstractListener;
import ru.t1.ktitov.tm.enumerated.Role;

import java.util.Arrays;
import java.util.Collection;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected AbstractListener[] abstractListeners;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected ISystemEndpoint getSystemEndpoint() {
        return serviceLocator.getSystemEndpoint();
    }

    @NotNull
    protected Collection<AbstractListener> getListeners() {
        return Arrays.asList(abstractListeners);
    }

}
