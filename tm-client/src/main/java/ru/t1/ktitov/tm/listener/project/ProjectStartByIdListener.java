package ru.t1.ktitov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.event.ConsoleEvent;
import ru.t1.ktitov.tm.util.TerminalUtil;

@Component
public final class ProjectStartByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start project by id";

    @Override
    @EventListener(condition = "@projectStartByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setProjectId(id);
        request.setStatus(Status.IN_PROGRESS);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
