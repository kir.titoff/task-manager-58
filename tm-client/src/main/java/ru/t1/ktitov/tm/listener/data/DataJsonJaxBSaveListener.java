package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataJsonJaxBSaveRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataJsonJaxBSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file by jaxb";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonJaxBSaveListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataJsonJaxBSaveRequest request = new DataJsonJaxBSaveRequest(getToken());
        domainEndpoint.saveDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
